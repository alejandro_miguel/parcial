﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entity
{
   public class InvoicesEntity
    {
        public int Id { get; set; }
        public int ClientId { get; set; }
        public ClientsEntity Clients { get; set; }
        public List<InvoiceDetailEntity> Detail { get; set; }
        public decimal Iva { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Total { get; set; }


        public InvoicesEntity (){
            Detail = new List<InvoiceDetailEntity>();
        }


    }
}
