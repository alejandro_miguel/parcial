﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entity
{
   public class InvoiceDetailEntity
    {
        public decimal Total;

        public int Id { get; set; }
        public int ProductId { get; set; }
        public ProductsEntity Product { get; set; }
        public int InvoiceId { get; set; }
        public InvoicesEntity Invoices { get; set; }

        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Iva { get; set; }
        public decimal SubTotal { get; set; }

    }
}
