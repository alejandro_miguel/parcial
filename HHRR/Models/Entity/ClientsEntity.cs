﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Entity
{
   public class ClientsEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string NumberPhone { get; set; }
        public string Address { get; set; }

    }
}
