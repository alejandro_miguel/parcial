﻿using Commons;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Services
{
  public class ProductServices
    {


        public List<ProductsEntity> GetAll()
        {
            var result = new List<ProductsEntity>();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var common = new SqlCommand("Select * from Products", context);

                using (var reader = common.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var prod = new ProductsEntity
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Name = Convert.ToString(reader["name"]),
                            Price = Convert.ToDecimal(reader["price"]),
                        
                        };
                        result.Add(prod);
                    }
                }

            }
            return result;
        }
        public ProductsEntity Get(int id)
        {
            var result = new ProductsEntity();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var command = new SqlCommand($"select * from Products where id = @id", context);
                command.Parameters.AddWithValue("@id", id);

                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    result.Id = Convert.ToInt32(reader["id"]);
                    result.Name = Convert.ToString(reader["name"]);
                    result.Price = Convert.ToDecimal(reader["price"]);
                  

                }          
            }



            return result;
        }

        public ProductsEntity GetName(string Name)
        {
            var result = new ProductsEntity();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var command = new SqlCommand($"select * from Products where name = @name", context);
                command.Parameters.AddWithValue("@name", Name);

                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    result.Id = Convert.ToInt32(reader["id"]);
                    result.Name = Convert.ToString(reader["name"]);
                    result.Price = Convert.ToDecimal(reader["price"]);


                }
            }



            return result;
        }
        public void Create(ProductsEntity model)
        {

            using (var transaction = new TransactionScope())
            {
                using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
                {
                    context.Open();
                    //Header
                    AddClient(model, context);
                    //Detail

                }
                transaction.Complete();
            }


        }
        public void Update(ProductsEntity model)
        {

            using (var transaction = new TransactionScope())
            {
                using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
                {
                    context.Open();
                    //Header
                    UpdateClient(model, context);

                }
                transaction.Complete();
            }

        }
        public bool Detele(int productId)
        {

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                try
                {
                    context.Open();
                    //Header
                    var command = new SqlCommand("Delete from Products where id =@id", context);
                    command.Parameters.AddWithValue("@id", productId);
                    command.ExecuteNonQuery();
                    return true;
                }
                catch (Exception)
                {

                    return false;
                    
                }
             
            }
        
        }
        private void AddClient(ProductsEntity products, SqlConnection context)
        {
            var query = "insert into Products ( name ,price) output INSERTED.Id values( @name,@price)";
            var command = new SqlCommand(query, context);
            command.Parameters.AddWithValue("@name", products.Name);
            command.Parameters.AddWithValue("@price", products.Price);
            products.Id = Convert.ToInt32(command.ExecuteScalar());
        }
        private void UpdateClient(ProductsEntity products, SqlConnection context)
        {
            var query = "update products set name = @name,price= @price  where id = @id";
            var command = new SqlCommand(query, context);

            command.Parameters.AddWithValue("@id", products.Id);
            command.Parameters.AddWithValue("@name", products.Name);
            command.Parameters.AddWithValue("@price", products.Price);
       
            command.ExecuteNonQuery();
        }
      
    }
}
