﻿using Commons;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Services
{
    public class InvoiceServices
    {

        public List<InvoicesEntity> GetAll()
        {
            var result = new List<InvoicesEntity>();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var common = new SqlCommand("Select * from invoices", context);

                using (var reader = common.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var invo = new InvoicesEntity
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Total = Convert.ToDecimal(reader["total"]),
                            SubTotal = Convert.ToDecimal(reader["subtotal"]),
                            Iva = Convert.ToDecimal(reader["iva"]),
                            ClientId = Convert.ToInt32(reader["clientid"])

                        };
                        result.Add(invo);
                    }
                }

                //set aditional propeties
                foreach (var invoice in result)
                {
                    //client
                    SetClient(invoice, context);
                    //Detail
                    SetDetail(invoice, context);
                }

            }
            return result;
        }

        public List<InvoicesEntity> GetInvoice()
        {
            var result = new List<InvoicesEntity>();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var common = new SqlCommand("Select * from invoices", context);

                using (var reader = common.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var invo = new InvoicesEntity
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Total = Convert.ToDecimal(reader["total"]),
                            SubTotal = Convert.ToDecimal(reader["subtotal"]),
                            Iva = Convert.ToDecimal(reader["iva"]),
                            ClientId = Convert.ToInt32(reader["clientid"])
                        };
                        result.Add(invo);
                    }
                }      
            }
            return result;
        }
        public InvoicesEntity Get(int id)
        {
            var result = new InvoicesEntity();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var command = new SqlCommand($"select * from Invoices where id = @id", context);
                command.Parameters.AddWithValue("@id", id);

                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    result.Id = Convert.ToInt32(reader["id"]);
                    result.Iva = Convert.ToDecimal(reader["iva"]);
                    result.SubTotal = Convert.ToDecimal(reader["subtotal"]);
                    result.Total = Convert.ToDecimal(reader["total"]);
                    result.ClientId = Convert.ToInt32(reader["clientid"]);

                }
                //client
                SetClient(result, context);
                //detail
                SetDetail(result, context);

            }



            return result;
        }

        public InvoicesEntity GetInvoice(int id)
        {
            var result = new InvoicesEntity();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var command = new SqlCommand($"select * from Invoices where id = @id", context);
                command.Parameters.AddWithValue("@id", id);

                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    result.Id = Convert.ToInt32(reader["id"]);
                    result.Iva = Convert.ToDecimal(reader["iva"]);
                    result.SubTotal = Convert.ToDecimal(reader["subtotal"]);
                    result.Total = Convert.ToDecimal(reader["total"]);
                    result.ClientId = Convert.ToInt32(reader["clientid"]);
                }          
            }
            return result;
        }
        public void Create(InvoicesEntity model)
        {
            PrepareOrder(model);
            using (var transaction = new TransactionScope()) 
            {
                using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
                {
                    context.Open();
                    //Header
                    AddHeader(model, context);
                    //Detail
                    AddDetail(model, context);
                }
                transaction.Complete();
            }

       
        }
        public void Update(InvoicesEntity model)
        {
            PrepareOrder(model);
            using (var transaction = new TransactionScope()) 
            {
                using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
                {
                    context.Open();
                    //Header
                    UpdateHeader(model, context);
                    //Remove detail
                    RemoveDetail(model.Id, context);
                    // Add UpdateDetail
                    AddDetail(model, context);
                }
                transaction.Complete();
            }
          
        }
        public bool Detele(int invoiceId)
        {
          
            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                //Header
                var command = new SqlCommand("Delete from invoices where id =@id", context);
                command.Parameters.AddWithValue("@id", invoiceId);
                command.ExecuteNonQuery();
            }
            return true;
        }

        #region Metodo Encapsulado 
        //Beggin ADD,Update the invoice with details
        //invoice 
        private void AddHeader(InvoicesEntity invoices,SqlConnection context) 
        {
            var query = "insert into Invoices (clientid, iva, subtotal, total) output INSERTED.Id values(@clientid,@iva,@subtotal,@total)";
            var command = new SqlCommand(query,context);
            command.Parameters.AddWithValue("@clientid",invoices.ClientId);
            command.Parameters.AddWithValue("@iva", invoices.Iva);
            command.Parameters.AddWithValue("@subtotal", invoices.SubTotal);
            command.Parameters.AddWithValue("@total", invoices.Total);

            invoices.Id = Convert.ToInt32(command.ExecuteScalar());
        }  
        private void AddDetail(InvoicesEntity invoices, SqlConnection context)
        {
            foreach (var detail in invoices.Detail) 
            {

                var query = "insert into InvoiceDetails (invoiceId,productId, quantity, price, iva, subtotal, total) values(@invoiceId,@productId, @quantity, @price, @iva, @subtotal, @total)";
                var command = new SqlCommand(query, context);
                command.Parameters.AddWithValue("@invoiceId", invoices.Id);
                command.Parameters.AddWithValue("@productId", detail.ProductId);
                command.Parameters.AddWithValue("@quantity", detail.Quantity);
                command.Parameters.AddWithValue("@price", detail.Price);
                command.Parameters.AddWithValue("@iva", detail.Iva);
                command.Parameters.AddWithValue("@subtotal", detail.SubTotal);
                command.Parameters.AddWithValue("@total", detail.Total);
               

                command.ExecuteNonQuery();
            }
        }
        private void UpdateHeader(InvoicesEntity invoices, SqlConnection context)
        {
            var query = "update Invoices set clientId = @clientId, iva = @iva,subtotal= @subtotal,total = @total where id = @id";
            var command = new SqlCommand(query, context);

            command.Parameters.AddWithValue("@clientid", invoices.ClientId);
            command.Parameters.AddWithValue("@iva", invoices.Iva);
            command.Parameters.AddWithValue("@subtotal", invoices.SubTotal);
            command.Parameters.AddWithValue("@total", invoices.Total);
            command.Parameters.AddWithValue("@id", invoices.Id);
            command.ExecuteNonQuery();
        }
        private void RemoveDetail(int invoiceId,SqlConnection context)
        {
           

                var query = "delete from Invoicedetails WHERE invoiceId = @invoiceId";
                var command = new SqlCommand(query, context);
                command.Parameters.AddWithValue("@invoiceId",invoiceId);
                command.ExecuteNonQuery();
            
        }
        private void PrepareOrder(InvoicesEntity model) 
        {
            foreach (var detail in model.Detail) 
            {
                detail.Total = detail.Quantity * detail.Price;
                detail.Iva = detail.Total * ParametreConnSQL.IVARate;
                detail.SubTotal = detail.Total - detail.Iva;
            }
            model.Total = model.Detail.Sum(x => x.Total);
            model.Iva = model.Detail.Sum(x => x.Iva);
            model.SubTotal = model.Detail.Sum(x=>x.SubTotal);
        }
        //end
        //Esto metodo encapsulado nos ayudad a realizar un mapper 
        private void SetClient(InvoicesEntity invoices, SqlConnection connection)
        {
            var command = new SqlCommand("select * from Clients where id = @clientid", connection);
            command.Parameters.AddWithValue("@clientid", invoices.ClientId);

            using (var reader = command.ExecuteReader())
            {
                reader.Read();
                invoices.Clients = new ClientsEntity
                {
                    Id = Convert.ToInt32(reader["id"]),
                    Name = reader["name"].ToString()
                };
            }

        }
        private void SetDetail(InvoicesEntity invoices, SqlConnection context)
        {
            var command = new SqlCommand($"SELECT * FROM InvoiceDetails where invoiceid = @id", context);
            command.Parameters.AddWithValue("@id", invoices.Id);

            using (var reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    invoices.Detail.Add(new InvoiceDetailEntity
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        InvoiceId = Convert.ToInt32(reader["invoiceid"]),
                        Price = Convert.ToDecimal(reader["price"]),
                        Quantity = Convert.ToInt32(reader["quantity"]),
                        SubTotal = Convert.ToDecimal(reader["subtotal"]),
                        Iva = Convert.ToDecimal(reader["iva"]),
                        ProductId = Convert.ToInt32(reader["productid"]),
                        Invoices = invoices
                    });
                }

            }
            //Set product
            foreach (var detail in invoices.Detail)
            {
                SetProduct(detail, context);
            }

        }
        private void SetProduct(InvoiceDetailEntity detail, SqlConnection context)
        {
            var command = new SqlCommand($"select * from Products where id = @productid", context);
            command.Parameters.AddWithValue("@productid", detail.ProductId);

            using (var reader = command.ExecuteReader())
            {
                reader.Read();
                detail.Product = new ProductsEntity
                {

                    Id = Convert.ToInt32(reader["id"]),
                    Name = reader["name"].ToString(),
                    Price = Convert.ToDecimal(reader["price"])
                };
            }

        }
        #endregion
    }




}
