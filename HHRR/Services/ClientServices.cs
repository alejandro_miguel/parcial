﻿using Commons;
using Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Services
{
    public class ClientServices
    {



        public List<ClientsEntity> GetAll()
        {
            var result = new List<ClientsEntity>();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var common = new SqlCommand("Select * from Clients", context);

                using (var reader = common.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var invo = new ClientsEntity
                        {
                            Id = Convert.ToInt32(reader["id"]),
                            Name = Convert.ToString(reader["name"]),
                            LastName = Convert.ToString(reader["lastname"]),
                            NumberPhone = Convert.ToString(reader["numberphone"]),
                            Address = Convert.ToString(reader["address"])

                        };
                        result.Add(invo);
                    }
                }

            }
            return result;
        }
        public ClientsEntity Get(int id)
        {
            var result = new ClientsEntity();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var command = new SqlCommand($"select * from Clients where id = @id", context);
                command.Parameters.AddWithValue("@id", id);

                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    result.Id = Convert.ToInt32(reader["id"]);
                    result.Name = Convert.ToString(reader["name"]);
                    result.LastName = Convert.ToString(reader["lastname"]);
                    result.NumberPhone = Convert.ToString(reader["numberphone"]);
                    result.Address = Convert.ToString(reader["address"]);

                }          
            }



            return result;
        }

        public ClientsEntity GetName(string Name)
        {
            var result = new ClientsEntity();

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                var command = new SqlCommand($"select * from Clients where name = @name", context);
                command.Parameters.AddWithValue("@name", Name);

                using (var reader = command.ExecuteReader())
                {
                    reader.Read();
                    result.Id = Convert.ToInt32(reader["id"]);
                    result.Name = Convert.ToString(reader["name"]);
                    result.LastName = Convert.ToString(reader["lastname"]);
                    result.NumberPhone = Convert.ToString(reader["numberphone"]);
                    result.Address = Convert.ToString(reader["address"]);
                }
            }



            return result;
        }


        public void Create(ClientsEntity model)
        {

            using (var transaction = new TransactionScope())
            {
                using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
                {
                    context.Open();
                    //Header
                    AddClient(model, context);
                    //Detail

                }
                transaction.Complete();
            }


        }
        public void Update(ClientsEntity model)
        {

            using (var transaction = new TransactionScope())
            {
                using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
                {
                    context.Open();
                    //Header
                    UpdateClient(model, context);

                }
                transaction.Complete();
            }

        }
        public bool Detele(int clientId)
        {

            using (var context = new SqlConnection(ParametreConnSQL.ParametreConnect))
            {
                context.Open();
                //Header
                var command = new SqlCommand("Delete from clients where id =@id", context);
                command.Parameters.AddWithValue("@id", clientId);
                command.ExecuteNonQuery();
            }
            return true;
        }
        private void AddClient(ClientsEntity clients, SqlConnection context)
        {
            var query = "insert into clients ( name ,lastname, numberphone ,address) output INSERTED.Id values( @name,@lastname,@numberphone,@address)";
            var command = new SqlCommand(query, context);
            command.Parameters.AddWithValue("@name", clients.Name);
            command.Parameters.AddWithValue("@lastname", clients.LastName);
            command.Parameters.AddWithValue("@numberphone", clients.NumberPhone);
            command.Parameters.AddWithValue("@address", clients.Address);

            clients.Id = Convert.ToInt32(command.ExecuteScalar());
        }
        private void UpdateClient(ClientsEntity clients, SqlConnection context)
        {
            var query = "update clients set name = @name,lastname= @lastname,numberphone = @numberphone,address = @address where id = @id";
            var command = new SqlCommand(query, context);

            command.Parameters.AddWithValue("@id", clients.Id);
            command.Parameters.AddWithValue("@name", clients.Name);
            command.Parameters.AddWithValue("@lastname", clients.LastName);
            command.Parameters.AddWithValue("@numberphone", clients.NumberPhone);
            command.Parameters.AddWithValue("@address", clients.Address);
            command.ExecuteNonQuery();
        }
        private void SetClient(ClientsEntity invoices, SqlConnection connection)
        {
            var command = new SqlCommand("select * from Clients where id = @clientid", connection);
            command.Parameters.AddWithValue("@clientid", invoices.Id);

            using (var reader = command.ExecuteReader())
            {
                reader.Read();
                invoices = new ClientsEntity
                {
                    Id = Convert.ToInt32(reader["id"]),
                    Name = reader["name"].ToString()
                };
            };

        }
    }
}

       

    






